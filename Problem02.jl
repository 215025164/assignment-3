### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ dc668a75-a60d-4832-87d4-4d03d85ca4f3
using Pkg

# ╔═╡ d9879a7b-044f-4a56-ab12-efe3b5a7a23b
Pkg.activate("Project.toml")

# ╔═╡ d351c4a1-2f51-4910-9b35-ba88b5118d37
using PlutoUI

# ╔═╡ b2de0335-1594-42ae-b741-a568906f1fd9
md"## Game"

# ╔═╡ 30559920-d8d2-11eb-28a5-7f4d943b4afc
const winningPositions = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], 
    [2, 5, 8], [3, 6, 9],[1, 5, 9], [7, 5, 3]]

# ╔═╡ e5907a81-52ac-4e63-8079-80ec7309c2b8
function haswon(board, player)
    marked = findall(x -> x == player, board)
    for pos in winningPositions
        if length(pos) <= length(marked) && pos == sort(marked)[1:3]
            return true
        end
    end
    false
end

# ╔═╡ 6e7d71c9-bebd-4ab5-ad3e-c1356f461de2
function readcharwithprompt(prompt, expected)
	ret = '*'
	while !(ret in expected)
		print("\n", prompt, " ->  ")
		ret = lowercase(chomp(readline()))[1]
		end
	ret
	end

# ╔═╡ 43ef1a6e-ce23-4d7d-aa43-d69e98b888a6

with_terminal() do
	readcharwithprompt(prompt, expected)
end

# ╔═╡ 92e43e33-faad-42f7-82ad-c6c7cf5505a1
begin
	availablemoves(brd) = findall(x -> x == ' ', brd)
    cornersopen(brd) = [x for x in [1, 3, 7, 9] if brd[x] == ' ']
    int2char(x) = Char(x + UInt8('0'))
    char2int(x) = UInt8(x) - UInt8('0')
    getyn(query) = readcharwithprompt(query, ['y', 'n'])
    gettheirmove(brd) = char2int(readcharwithprompt("Your move(1-9)", int2char.           (availablemoves(brd))))
end

# ╔═╡ 90101a34-744c-41fc-a532-a93828f64fdc
function findwin(brd, xoro)
    tmpbrd = deepcopy(brd)
    for mv in availablemoves(tmpbrd)
        tmpbrd[mv] = xoro
        if haswon(tmpbrd, xoro)
            return mv
        end
        tmpbrd[mv] = ' '
    end
    return nothing
end

# ╔═╡ 7c559c84-a7f7-4e1c-b12f-b24dee86b114
function choosemove(brd, mychar, theirchar)
    if all(x -> x == ' ', brd)
        brd[rand(cornersopen(brd))] = mychar 
    elseif availablemoves(brd) == [] # no more moves
        println("Game is over. It was a draw.")
        exit(0)
    elseif (x = findwin(brd, mychar)) != nothing || (x = findwin(brd, theirchar)) != nothing
        brd[x] = mychar 
    elseif brd[5] == ' '
        brd[5] = mychar 
    elseif (corners = cornersopen(brd)) != []
        brd[rand(corners)] = mychar 
    else
        brd[rand(availablemoves(brd))] = mychar 
    end
end

# ╔═╡ fc283874-2800-4e1f-8371-62072f219f25
function display(brd)
    println("+-----------+")
    println("| ", brd[1], " | ", brd[2], " | ", brd[3], " |")
    println("| ", brd[4], " | ", brd[5], " | ", brd[6], " |")
    println("| ", brd[7], " | ", brd[8], " | ", brd[9], " |")
    println("+-----------+")
end

# ╔═╡ 85ce4c01-515e-4710-9875-b4f0af33d5aa
function game()
    board = fill(' ', 9)
    println("Board move grid:\n 1 2 3\n 4 5 6\n 7 8 9")
    yn = getyn("Would you like to move first (y/n)?")
    if yn == 'y'
        mychar = 'O'
        theirchar = 'X'
        board[gettheirmove(board)] = theirchar
    else
        mychar = 'X'
        theirchar = 'O'
    end
    while true
        choosemove(board, mychar, theirchar)
        println("Computer has moved.")
        display(board)
        if haswon(board, mychar)
            println("Game over. Computer wins!")
            exit(0)
        elseif availablemoves(board) == []
            break
        end
        board[gettheirmove(board)] = theirchar
        println("Player has moved.")
        display(board)
        if haswon(board, theirchar)
            println("Game over. Player wins!")
            exit(0)
        elseif availablemoves(board) == []
            break
        end
    end
    println("Game over. It was a draw.")
end

# ╔═╡ 196136d8-bbb8-464c-be18-78e0c1f5ed14
with_terminal() do
	tictactoe()
end

# ╔═╡ Cell order:
# ╠═b2de0335-1594-42ae-b741-a568906f1fd9
# ╠═dc668a75-a60d-4832-87d4-4d03d85ca4f3
# ╠═d9879a7b-044f-4a56-ab12-efe3b5a7a23b
# ╠═d351c4a1-2f51-4910-9b35-ba88b5118d37
# ╠═30559920-d8d2-11eb-28a5-7f4d943b4afc
# ╠═e5907a81-52ac-4e63-8079-80ec7309c2b8
# ╠═6e7d71c9-bebd-4ab5-ad3e-c1356f461de2
# ╠═43ef1a6e-ce23-4d7d-aa43-d69e98b888a6
# ╠═92e43e33-faad-42f7-82ad-c6c7cf5505a1
# ╠═90101a34-744c-41fc-a532-a93828f64fdc
# ╠═7c559c84-a7f7-4e1c-b12f-b24dee86b114
# ╠═fc283874-2800-4e1f-8371-62072f219f25
# ╠═85ce4c01-515e-4710-9875-b4f0af33d5aa
# ╠═196136d8-bbb8-464c-be18-78e0c1f5ed14
