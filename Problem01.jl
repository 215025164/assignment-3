### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ d01a5edd-863f-484b-b923-6d7684f09ada
using Pkg

# ╔═╡ 6bbd73d2-8a32-48f5-9dad-24f2b3654dc7
Pkg.activate("Project.toml")

# ╔═╡ 7fd750df-547d-4036-b1af-3a8a4e92d938
using LinearAlgebra, Statistics, BenchmarkTools, Plots, QuantEcon

# ╔═╡ 5eea20f4-cb15-432d-904d-ae7ec2a3a081
using SparseArrays

# ╔═╡ b318ffe4-dd5d-45b6-bc3e-3f0721d728ce
using Parameters

# ╔═╡ 088f5850-5302-4cff-9f69-d627b428dc0b
gr(fmt = :png);

# ╔═╡ 9cf451d3-8d27-47e3-9f66-89c4469e8b62
SimpleOG = @with_kw (B = 10, M = 5, α = 0.5, β = 0.9)

# ╔═╡ c1b55d9b-8856-4031-bcb0-5768fd9910ed
function transition_matrices(g)
    @unpack B, M, α, β = g
    u(c) = c^α
    n = B + M + 1
    m = M + 1

    R = zeros(n, m)
    Q = zeros(n, m, n)

    for a in 0:M
        Q[:, a + 1, (a:(a + B)) .+ 1] .= 1 / (B + 1)
        for s in 0:(B + M)
            R[s + 1, a + 1] = (a≤s ? u(s - a) : -Inf)
        end
    end

    return (Q = Q, R = R)
end

# ╔═╡ 9aeabda1-4241-4604-977f-9ab8abecc4bf
g = SimpleOG();

# ╔═╡ 214f0e0d-d998-4121-8445-9eda76120a11
Q, R = transition_matrices(g);

# ╔═╡ 7cdad05e-7f73-4b8b-8ef2-ea9486c99970
function verbose_matrices(g)
    @unpack B, M, α, β = g
    u(c) = c^α

    #Matrix dimensions. The +1 is due to the 0 state.
    n = B + M + 1
    m = M + 1

    R = fill(-Inf, n, m) #Start assuming nothing is feasible
    Q = zeros(n,m,n) #Assume 0 by default

    #Create the R matrix
    #Note: indexing into matrix complicated since Julia starts indexing at 1 instead of 0
    #but the state s and choice a can be 0
    for a in 0:M
         for s in 0:(B + M)
            if a <= s #i.e. if feasible
                R[s + 1, a + 1] = u(s - a)
            end
        end
    end

    #Create the Q multi-array
    for s in 0:(B+M) #For each state
        for a in 0:M #For each action
            for sp in 0:(B+M) #For each state next period
                if( sp >= a && sp <= a + B) # The support of all realizations
                    Q[s + 1, a + 1, sp + 1] = 1 / (B + 1) # Same prob of all
                end
            end
            @assert sum(Q[s + 1, a + 1, :]) ≈ 1 #Optional check that matrix is stochastic
         end
    end
    return (Q = Q, R = R)
end

# ╔═╡ f962eacd-aa9b-4c79-97a4-7a69d9b25b82
ddp = DiscreteDP(R, Q, g.β);

# ╔═╡ 8a4ea5c6-d49f-4fad-a73b-1d717f87d612
results = solve(ddp, PFI)

# ╔═╡ 7b525fe6-22b4-4a3d-ac76-107d5d47d9ad
fieldnames(typeof(results))

# ╔═╡ 29647f5d-c7cb-4bef-ba29-4732fdfa930e
results.v

# ╔═╡ ee3b20a7-7e99-4084-923b-bb057f742dbe
results.sigma .- 1

# ╔═╡ 6117cefd-615f-438e-add6-f1c769664fe3
results.num_iter

# ╔═╡ 056238a0-8ed6-4fde-a397-1f05d5e547bd
stationary_distributions(results.mc)[1]


# ╔═╡ ef3db783-4225-47ad-bddf-861b6cb4b491
begin
	g_2 = SimpleOG(β=0.99);
    Q_2, R_2 = transition_matrices(g_2);

    ddp_2 = DiscreteDP(R_2, Q_2, g_2.β)

    results_2 = solve(ddp_2, PFI)

    std_2 = stationary_distributions(results_2.mc)[1]
end

# ╔═╡ 05592540-bcc7-48fb-ae17-00221937e8c0
function compute_policies(n_vals...)
    c_policies = []
    w = w_init
    for n in 1:maximum(n_vals)
        w = bellman_operator(ddp, w)
        if n in n_vals
            σ = compute_greedy(ddp, w)
            c_policy = f(grid) - grid[σ]
            push!(c_policies, c_policy)
        end
    end
    return c_policies
end

# ╔═╡ Cell order:
# ╠═d01a5edd-863f-484b-b923-6d7684f09ada
# ╠═6bbd73d2-8a32-48f5-9dad-24f2b3654dc7
# ╠═7fd750df-547d-4036-b1af-3a8a4e92d938
# ╠═5eea20f4-cb15-432d-904d-ae7ec2a3a081
# ╠═b318ffe4-dd5d-45b6-bc3e-3f0721d728ce
# ╠═088f5850-5302-4cff-9f69-d627b428dc0b
# ╠═9cf451d3-8d27-47e3-9f66-89c4469e8b62
# ╠═c1b55d9b-8856-4031-bcb0-5768fd9910ed
# ╠═9aeabda1-4241-4604-977f-9ab8abecc4bf
# ╠═214f0e0d-d998-4121-8445-9eda76120a11
# ╠═7cdad05e-7f73-4b8b-8ef2-ea9486c99970
# ╠═f962eacd-aa9b-4c79-97a4-7a69d9b25b82
# ╠═8a4ea5c6-d49f-4fad-a73b-1d717f87d612
# ╠═7b525fe6-22b4-4a3d-ac76-107d5d47d9ad
# ╠═29647f5d-c7cb-4bef-ba29-4732fdfa930e
# ╠═ee3b20a7-7e99-4084-923b-bb057f742dbe
# ╠═6117cefd-615f-438e-add6-f1c769664fe3
# ╠═056238a0-8ed6-4fde-a397-1f05d5e547bd
# ╠═ef3db783-4225-47ad-bddf-861b6cb4b491
# ╠═05592540-bcc7-48fb-ae17-00221937e8c0
